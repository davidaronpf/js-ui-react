import React from "react";
import { render, screen } from "@testing-library/react";
import Orders from "./Orders";
import Deposits from "./Deposits";
import Bitcoin from "./Bitcoin";
import Dogfacts from "./Dogfacts";

const mockData = {
  time: {
    updated: "Sep 24, 2023 21:39:00 UTC",
    updatedISO: "2023-09-24T21:39:00+00:00",
    updateduk: "Sep 24, 2023 at 22:39 BST",
  },
  disclaimer:
    "This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
  chartName: "Bitcoin",
  bpi: {
    USD: {
      code: "USD",
      symbol: "&#36;",
      rate: "26,501.9073",
      description: "United States Dollar",
      rate_float: 26501.9073,
    },
    GBP: {
      code: "GBP",
      symbol: "&pound;",
      rate: "22,144.7817",
      description: "British Pound Sterling",
      rate_float: 22144.7817,
    },
    EUR: {
      code: "EUR",
      symbol: "&euro;",
      rate: "25,816.7270",
      description: "Euro",
      rate_float: 25816.727,
    },
  },
};

test("renders orders", () => {
  render(<Orders />);
  const title = screen.getByText(/recent orders/i);
  expect(title).toBeInTheDocument();
});

test("renders deposits", () => {
  render(<Deposits />);

  const title = screen.getByText(/recent deposits/i);
  expect(title).toBeInTheDocument();
});

test("renders bitcoin section", async () => {
  render(<Bitcoin cont={mockData} />);

  expect(
    await screen.findByText(`Time Updated: ${mockData.time.updated}`)
  ).toBeInTheDocument();

  expect(await screen.findByText(mockData.bpi.USD.code)).toBeInTheDocument();
  expect(await screen.findByText(mockData.bpi.GBP.code)).toBeInTheDocument();
  expect(await screen.findByText(mockData.bpi.EUR.code)).toBeInTheDocument();

  expect(await screen.findByText(mockData.disclaimer)).toBeInTheDocument();
});

test("renders dog facts section", async () => {
  render(<Dogfacts />);

  expect(await screen.findByText(`Dog Facts`)).toBeInTheDocument();
});
