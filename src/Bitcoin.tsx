import * as React from "react";
import Title from "./Title";

import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import { Typography } from "@mui/material";

export default function Bitcoin(props: { cont: any }) {
  return (
    props.cont && (
      <React.Fragment>
        <Typography variant="body2" color="text.secondary" align="right">
          {props.cont.time && `Time Updated: ${props.cont.time.updated}`}
        </Typography>
        <Grid container>
          {props.cont.bpi &&
            Object.keys(props.cont.bpi).map((label: string, index: number) => (
              <Grid item xs={12} sm={4} key={label}>
                <Card variant="outlined">
                  <Title>{props.cont.bpi[label].code}</Title>
                  <Typography>{props.cont.bpi[label].description}</Typography>
                  <Typography color="green">
                    ${" "}
                    {
                      props.cont.bpi[label].rate.slice(
                        0,
                        props.cont.bpi[label].rate.indexOf(".") + 2 + 1
                      ) /* Slices rate to two characters after dot without altering rest of string or rounding */
                    }
                  </Typography>
                </Card>
              </Grid>
            ))}
        </Grid>
        <Typography variant="body2" color="text.secondary" align="center">
          {props.cont.disclaimer && props.cont.disclaimer}
        </Typography>
      </React.Fragment>
    )
  );
}
