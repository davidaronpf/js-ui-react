import * as React from "react";
import axios from "axios";
import Link from "@mui/material/Link";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import Title from "./Title";
import Card from "@mui/material/Card";

export default function Dogfacts() {
  const [loading, setLoading] = React.useState(true);
  const [dogFacts, setDogFacts] = React.useState({
    current_page: 0,
    data: [],
    last_page: 0,
  });

  const fetchDogFacts = (page: number = 1) => {
    setLoading(true);
    axios
      .get("http://localhost:8042/api/v1/dog-facts?page=" + page)
      .then(({ data }) => {
        setDogFacts(data);
        setLoading(false);
      });
  };

  React.useEffect(() => {
    fetchDogFacts();
  }, []);

  return (
    <React.Fragment>
      <Title>Dog Facts</Title>
      {loading ? (
        <CircularProgress />
      ) : (
        dogFacts.data &&
        dogFacts.data.map((fact: { fact: String }, index: number) => (
          <Card variant="outlined" key={index}>
            <Typography>{fact.fact}</Typography>
          </Card>
        ))
      )}
      <Typography align="right" sx={{ m: 0.5 }}>
        {dogFacts.current_page > 0 && `Page ${dogFacts.current_page} `}
        {dogFacts.current_page > 1 && (
          <Link
            color="primary"
            href="#"
            onClick={() => {
              fetchDogFacts(dogFacts.current_page - 1);
            }}
          >
            Previous
          </Link>
        )}{" "}
        {dogFacts.current_page !== dogFacts.last_page && (
          <Link
            color="primary"
            href="#"
            onClick={() => {
              fetchDogFacts(dogFacts.current_page + 1);
            }}
          >
            Next
          </Link>
        )}
      </Typography>
    </React.Fragment>
  );
}
